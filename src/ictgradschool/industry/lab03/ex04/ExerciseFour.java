package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.
        String amount = readInputAmount();
        int decimalPlaces = readInputDecimalPlaces();
        String newAmount = truncateToDPS(amount, decimalPlaces);
        printNewAmount(newAmount);

    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    private String readInputAmount() {
        System.out.println("Enter the amount: ");
        String input = Keyboard.readInput();
        return input;
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    private int readInputDecimalPlaces() {
        System.out.println("Enter the decimal places: ");
        String input = Keyboard.readInput();
        int output = Integer.parseInt(input);
        return output;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    private String truncateToDPS(String amount, int demicalPlace) {
        int placeOfDot = amount.indexOf('.');
        String fractionalPart = amount.substring(placeOfDot + 1);
        String newAmount = amount.substring(0, placeOfDot + 1) + fractionalPart.substring(0, demicalPlace);
        return newAmount;
    }

    // TODO Write a method which prints the truncated amount
    private void printNewAmount(String newAmount) {
        System.out.println(newAmount);

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
