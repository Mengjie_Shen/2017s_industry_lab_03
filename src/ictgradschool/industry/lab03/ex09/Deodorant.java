package ictgradschool.industry.lab03.ex09;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info += " Deodorant, \n$" + price;
        return info;
    }

    // TODO Implement all methods below this line.

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isRollOn() {
        return rollOn;
    }

    public String getFragrance() {
        return fragrance;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setFragrance(String fragrance) {
        this.fragrance = fragrance;
    }

    public boolean isMoreExpensiveThan(Deodorant other) {
        double otherPrice = other.getPrice();
        return this.price > otherPrice;
    }

    public static void main(String[] args) {
        Deodorant myDeodorant = new Deodorant("Gentle", "Baby Powder", true, 4.99);
        Deodorant yourDeodorant = new Deodorant("Spring", "Blossom", false, 3.99);
        System.out.println(myDeodorant.getBrand());
        System.out.println(myDeodorant.getFragrance());
        System.out.println(myDeodorant.isRollOn());
        System.out.println(myDeodorant.getPrice());
        System.out.println(myDeodorant.isMoreExpensiveThan(yourDeodorant));
        System.out.println("****************************************************************");


        System.out.println("1. " + myDeodorant.toString());
        myDeodorant.setBrand("Sweet");
        yourDeodorant.setPrice(5.29);
        System.out.println("2. " + yourDeodorant.toString());
        if (myDeodorant.isRollOn()) {
            System.out.println("3. Roll On");
        } else {
            System.out.println("3. Spray");
        }
        System.out.println("4. " + myDeodorant.toString());
        if (yourDeodorant.isMoreExpensiveThan(myDeodorant)) {
            System.out.println("5. Most expensive is " +
                    yourDeodorant.getBrand());
        } else {
            System.out.println("5. Most expensive is " +
                    myDeodorant.getBrand());
        }

    }
}