package ictgradschool.industry.lab03.ex08;

import ictgradschool.Keyboard; // err 1: import keyboard

/**
 * Created by mshe666 on 13/11/2017.
 */

public class CalculateVolume { // err 2: missing  {
    public void start() { // err 3: missing "void"
        double radius; // err 4: missing ;
        System.out.println("\"Volume of a Sphere\""); // err 5: missing \
        System.out.println(" Enter the radius : "); // err 6: missing "
        radius = Double.parseDouble(Keyboard.readInput()); // err 7: missing () ;  err 8: change "Integer.parseInt()" method to "Double.parseDouble()"
        double volume = (double) 4 / 3 * Math.PI * Math.pow(radius, 3); // err 9: change "int" to "double"  ; err 10: cast to double  ;  err 11: 2  --> 3
        System.out.println("Volume: " + volume); // err 12: , --> +
    }

    public static void main(String[] args) { // err 13: missing main method
        CalculateVolume p = new CalculateVolume();
        p.start();
    }
}
