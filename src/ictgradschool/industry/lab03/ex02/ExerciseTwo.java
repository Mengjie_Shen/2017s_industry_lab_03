package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        int lowerBound = readInteger("Enter the lower bound:");
        int upperBound = readInteger("Enter the upper bound:");
        int number01 = lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1));
        int number02 = lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1));
        int number03 = lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1));
        int maxNum = Math.max(number01, Math.max(number02, number03));
        int minNum = Math.min(number01, Math.min(number02, number03));
        System.out.println("The three random numbers are: " + number01 + " " + number02 + " " + number03);
        System.out.println("The max number is: " + maxNum);
        System.out.println("The min number is: " + minNum);
    }

    private int readInteger(String prompt) {
        System.out.println(prompt);
        String input = Keyboard.readInput();
        int value = Integer.parseInt(input);
        return value;
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
